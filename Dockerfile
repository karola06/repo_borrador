# Imagen raíz
FROM node

#Carpeta raíz
WORKDIR /apitechu

#Copia de archivos de carpeta local a apitechu (que está en la imagen)
ADD . /apitechu

#Instalación de las dependencias (npm install)
#only=prod dependencias de dependecies no devDependecies
RUN npm install --only=prod

# Puerto de trabajo
EXPOSE 3000

# Comando de inicialización (node server.js)
CMD ["node", "server.js"]
