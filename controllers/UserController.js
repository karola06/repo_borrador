
const io = require ('../io');
const crypt = require("../crypt");


// para conectar con mlab
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechucab12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");
  console.log("Query string");
  console.log(req.query);

  var users = require('../usuarios.json');
//  res.send(users);

  var numUsuarios = req.query.$TOP;
  var contador = req.query.$COUNT;
//    if (numUsuarios == '') { no hace falta
//  Se puede preguntar directamente por el parametro del query string
  if (!req.query.$TOP) {
    if (contador != 'true'){
          var resultado = {
          "usuarios" : users
          }
    }
    else{
      var resultado = {
      "usuarios" : users,
      "contador" : users.length
      }
    }
  }
  else{
    if (contador != 'true'){
          var resultado = {
          "usuarios" : users.slice(0 , numUsuarios)
          }
    }
    else{
      var resultado = {
      "usuarios" : users.slice(0 , numUsuarios),
      "contador" : users.length,
      }
    }
  }

  res.send(resultado);
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMlab, body){
      var response = !err ? body :{
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

function getUserByIdV2 (req, res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  console.log("La id del usuario a obtener es: " + id);
  var query ='q={"id":' + Number.parseInt(id) +'}';

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");


// para provocar el error 500 se puede poner un slash a user:
//  httpClient.get("/user?" + query + "&" + mLabAPIKey,
  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMlab, body){
    // body devuelve un array
/*    var response = !err ? body[0] :{
      "msg" : "Error obteniendo usuario"
    }*/
    if (err) {
      var response = {
            "msg" : "Error obteniendo usuarios"
          }
      res.status(500);
      } else{
        if (body.length > 0){
          var response = body[0];
        } else{
          var response = {
                "msg" : "Usuario no encontrado"
              }
          res.status(404);
        }
    }
    res.send(response);
  }
);

}

function createUsersV1(req, res) {
  console.log("POST /apitechu/v1/users");

  //para enviar información en el body
  //Desde Postman se incluyen los datos de los usuarios en el body
  //y se recuperan en el servidor
  console.log(req.body);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
  }

  console.log(newUser);
  var users = require('../usuarios.json');
  //push es un método para añadir al final de un array
  users.push(newUser);
  console.log("usuario añadido al array");

  io.writeUserDataToFile(users);
  res.send({"msg":"usuario creado"});
}

function createUsersV2(req, res){
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  }

  var httpClient = requestJson.createClient(baseMLABUrl);

  httpClient.post("user?" + mLabAPIKey, newUser,
  function(err, resMlab, body){
    console.log("Usuario creado");
    res.status(201);
    res.send({"msg":"usuario creado"});
  }
  )
}

function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("La id del usuario a borrar es :");
  console.log(req.params.id);

  var users = require('../usuarios.json');
  var posicion = -1;

//    for normal  -----------------------------
/*  for (var i=0; i < users.length; i++){
    if (users[i].id == req.params.id){
      console.log("Usuario encontrado en la posición ", i);
      posicion = i;
      break;
    }
  } */

//  for in ----------------------------
/* for (var element in users){
      console.log("Elemento ", element);
      if (users[element].id == req.params.id){
        console.log("Usuario encontrado en la posición ", element);
        posicion = element;
        break;
      }
  } */

// forEach  ------------------------------
/*  var i = 0;
  users.forEach(function(element){
    console.log(element.id)
    if (element.id == req.params.id){
      posicion = i;
    }
    i++;
  }); */


// for of   --------------------------------
/* var i=0;
 for (element of users){
   if (element.id == req.params.id){
     posicion=i;
     break;
   }
   i++;
 }   */

// findIndex  --------------------------------------
posicion = users.findIndex(function(element){
  return element.id == req.params.id;
});

console.log("La posición a borrar es ", posicion);
if (posicion == -1) {
  res.send({"msg":"usuario no encontrado"});
}
else {
  users.splice(posicion, 1);
  io.writeUserDataToFile(users);
  res.send({"msg":"usuario borrado"});
}
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUsersV1 = createUsersV1;
module.exports.createUsersV2 = createUsersV2;
module.exports.deleteUserV1 = deleteUserV1;
