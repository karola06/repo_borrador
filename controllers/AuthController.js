const io = require ('../io');
const crypt = require("../crypt");

// para conectar con mlab
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechucab12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginUserV1 (req, res){
  console.log("LOGIN");
  var users = require('../usuarios.json');
  var resultado = {};

  console.log("Login");
  console.log(req.body.email);
  console.log(req.body.password);
  var posicion = users.findIndex(function(element){
    return (element.email == req.body.email && element.password == req.body.password) ;
  });
  if (posicion == -1){
    console.log("NO existe");
    resultado.mensaje = "Login incorrecto";
    res.send(resultado);
  }
  else {
    console.log("SI existe");
//    users[posicion].logged = "true";
    users[posicion].logged = true;
    resultado.mensaje = "Login correcto";
    resultado.idUsuario = users[posicion].id;
    console.log(resultado);
    io.writeUserDataToFile(users);
    res.send(resultado);
  }
}

function logoutUserV1 (req, res){
  console.log("LOGOUT");
  var users = require('../usuarios.json');
  var resultado = {};

  var posicion = users.findIndex(function(element){
//    return (element.id == req.params.id && element.logged == "true") ;
    return (element.id == req.params.id && element.logged === true) ;
  });
  if (posicion == -1){
    console.log("NO existe");
    resultado.mensaje = "Logout incorrecto";
    res.send(resultado);
  }
  else {
    console.log("SI existe");
    delete users[posicion].logged;
    resultado.mensaje = "Logout correcto";
    resultado.idUsuario = users[posicion].id;
    io.writeUserDataToFile(users);
    res.send(resultado);
  }
}

function loginUserV2(req, res) {
  console.log("GET /apitechu/v2/login");
  console.log("Login");
  console.log(req.body.email);
//  console.log(req.body.password);

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  // var query ='q={"email":{"$eq":"' + req.body.email +'"}}';
  var query = "q=" + JSON.stringify({"email": req.body.email});
  var response = {};
  console.log("query: " + query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab1, body){
        if (body.length > 0 ){
            if (crypt.checkPassword(req.body.password,body[0].password)){
              var putBody = '{"$set":{"logged":true}}';
  //            query ='q={"id":' + body[0].id + '}';
              query ='q={"id":' + Number.parseInt(body[0].id) + '}';  // Se convierte a entero es conveniente
              console.log("query: " + query);
              httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
                function(err, resMlab2, body2){
                  response = {"msg":"Usuario logado",
                              "idUsuario" : body[0].id
                            }
                  res.send(response);
                }
              )
            }
            else {
              response = {"msg":"Login incorrecto"}
              res.status(401);
              res.send(response);
            }
          } else{
            response = {"msg":"Login incorrecto"}
            res.status(401);
            res.send(response);
          }
      }
  );
}

function logoutUserV2(req, res) {
  console.log("GET /apitechu/v2/logout");
  var resultado = {};
  var httpClient = requestJson.createClient(baseMLABUrl);
//  var query ='q={"$and":[{"id":' + req.params.id + '} , {"logged":true}]}';
  var query ='q={"$and":[{"id":' + Number.parseInt(req.params.id) + '} , {"logged":true}]}';
  var response = {};
  console.log("Client created");
  console.log("query: " + query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab1, body){
        console.log("longitud " + body.length);
        if (body.length == 0 ){
          response = {"msg":"Logout incorrecto"}
          res.send(response);
        } else {
            query ='q={"id":' + req.params.id +'}}';
            var putBody = '{"$unset":{"logged":""}}'

            console.log("query: " + query);

            httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
              function(err, resMlab2, body){
                response = {"msg":"Usuario logout"};  //body[0]
                res.send(response);
              }
            )
        }
    }
  )
}

module.exports.loginUserV1 = loginUserV1;
module.exports.logoutUserV1 = logoutUserV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV2 = logoutUserV2;
