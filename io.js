//para escribir en disco
const fs = require('fs');

function writeUserDataToFile(data){
  console.log("writeUserDataToFile");

  //para escribir en el disco en JSON hay que serializar user con stringify
  var jsonUserData = JSON.stringify(data);

  fs.writeFile('./usuarios.json', jsonUserData, "utf-8",
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Usuario escrito en fichero")
      }
    }
  );
}

module.exports.writeUserDataToFile = writeUserDataToFile;
