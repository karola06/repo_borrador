//3 librerias *****//
const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');
//***********************//

chai.use(chaihttp);

// chai framework para excepciones
//should lo vamos a utilizar para hacer las aserciones
var should = chai.should();

describe("First test",
  function(){
    // it es un test unitario
    it('Test that duckduckgo works', function(done){
        chai.request('http://www.duckduckgo.com')
            .get('/')
            .end(
              //función manejadora que va a gestionar la respuesta
              function(err,res){
                console.log("Request finished");
                //console.log(res);
                // console.log(err);
                done(); //sirve para indicarle al Framework cuando tiene que comprobar las aserciones
              }
            )
      }
    )
  }
)

describe("Test de API de usuarios",
  function(){
    // it es un test unitario
    it('Tests that user api says hello', function(done){
        chai.request('http://localhost:3000')
            .get('/apitechu/v1/hello')
            .end(
              //función manejadora que va a gestionar la respuesta
              function(err,res){
                console.log("Request finished");
                //console.log(res);
                // console.log(err);
                res.should.have.status(200);
                res.body.msg.should.be.eql("Hola desde API Techu");  //campo msg de la respuesta be equal
                done(); //sirve para indicarle al Framework cuando tiene que comprobar las aserciones
              }
            )
      }
    ),
    it('Tests that user api returns user list', function(done){
        chai.request('http://localhost:3000')
            .get('/apitechu/v1/users')
            .end(
              //función manejadora que va a gestionar la respuesta
              function(err,res){
                console.log("Request finished");
                res.should.have.status(200);

                //comprobar que me esta devolviendo un arrary
                res.body.usuarios.should.be.a("array");

                //comprobar que un usuario sea correcto (tenga id, email y password)
                for (usuario of res.body.usuarios){
                  usuario.should.have.property("id");
                  usuario.should.have.property("email");
                  usuario.should.have.property("password");
                }
                done(); //sirve para indicarle al Framework cuando tiene que comprobar las aserciones
              }
            )
      }
    )
  }
)db




// Documento MongoDB  ******************************
{
  "first_name" : "Server",
  "last_name" : "iano",
  "quesos" : ["Dehesa de los Llanos", "Montescusa"],
  "amigotes" : [
    {
      "name" : "Manolo",
      "de_quien_es" : "el de Pistolas"
    }, {
      "name" : "Indalecio",
      "de_quie_es" : "el de Pelala"
    }
  ],
  "queso_favorito" : {
    "name" : "Dehesa de los Llanos",
    "milk" : "cabra"
  }
}
