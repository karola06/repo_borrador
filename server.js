require('dotenv').config();
const express = require('express');
const app = express();  //inicializa el framework y lo pone en app

var enableCORS = function(req, res, next) {

 res.set("Access-Control-Allow-Origin", "*");

 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");

//  aqui habría que añadir también el JWT  (Jason Web Token)

 next();

}

// en postman tengo que tenerlo también indicado en la cabecera JSON (content-type)
app.use(express.json());  //le indico a express que le estoy enviando las cosas en JSON
app.use(enableCORS);

//**********const io = require ('./io');

const userController = require('./controllers/UserController');

const authController = require('./controllers/AuthController');

const port = process.env.PORT || 3000; //le asigna por defecto 3000 al no estar definida

app.listen(port) //pone en marcha el servidor

console.log("API escucchando en el puerto BIP BIP " + port);

app.get("/apitechu/v1/hello",
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg":"Hola desde API Techu"}); //resultado en formato JSON
  }
)

// p1 y p2 son parámetros
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("Parámetros");
    console.log(req.params);
    console.log("Query string");
    console.log(req.query);
    console.log("Headers");
    console.log(req.headers);
    console.log("Body");
    console.log(req.body);
  }
)

app.get("/apitechu/v1/users",
  userController.getUsersV1
)

app.post("/apitechu/v1/users",
  userController.createUsersV1
)

app.delete("/apitechu/v1/users/:id",
  userController.deleteUserV1
)

app.post("/apitechu/v1/login",
  authController.loginUserV1
)

app.post("/apitechu/v1/logout/:id",
  authController.logoutUserV1
)

app.get("/apitechu/v2/users",
  userController.getUsersV2
)

app.get("/apitechu/v2/users/:id",
  userController.getUserByIdV2
)

app.post("/apitechu/v2/users",
  userController.createUsersV2
)

app.post("/apitechu/v2/login",
  authController.loginUserV2
)

app.post("/apitechu/v2/logout/:id",
  authController.logoutUserV2
)
